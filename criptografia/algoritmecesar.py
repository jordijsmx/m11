def encriptar(frase,taula):
    
    posicio = 0
    xifrat = ''

    for i in frase:
        posicio=0
        for j in taula:
            if i == j:
                xifrat+=taula[posicio+1]
            posicio+=1
        
    return xifrat


def desencriptar(frase,taula):
    posicio = 0
    solucio = ''

    for i in frase:
        posicio = 0
        for j in taula:
            if i == j:
                solucio+=taula[posicio-1]
            posicio+=1
    
    return solucio


taula = "abcdefghijklmnopqrstuvwxyz"

print(encriptar("hola",taula))
