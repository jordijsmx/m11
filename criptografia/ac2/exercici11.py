from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP

'''
Quina és la longitud de la clau privada de l’algoritme RSA? ( Mostra evidències) Quina és la
longitud de la clau pública de l’algoritme RSA? ( Mostra evidències) Implementa un exemple de com
generar un parell de claus RSA
'''

# Generar un parell de claus RSA
claus = RSA.generate(1024)

print("La longitud de la clau privada és:",len(claus.export_key()))
print("La longitud de la clau pública és:",len(claus.publickey().export_key()))