import hashlib
from sys import argv as arg

'''
Programa que donat un fitxer, el processa linea a linea, l'encripta amb SHA256 
i l'emmagatzema a un nou fitxer, respectant els salts de línea.
'''

def xifrat_sha256(text):

    hash = hashlib.sha256(text.encode())

    return hash.hexdigest()


fitxer = input("Introdueix la ruta absoluta del fitxer a encriptar: ")

try: 
    with open(fitxer,"r") as fitxer:

        linies = fitxer.readlines()

        with open("dades_encriptades.txt", 'w') as f:
            for i in linies:
                espais = i.strip()
                if espais:
                    hash = xifrat_sha256(espais)
                    f.write(hash + "\n")

except FileNotFoundError:

      print("Error: el fitxer no existeix o no es troba al directori introduït.")