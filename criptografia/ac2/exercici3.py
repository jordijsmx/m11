import hashlib

'''
Modifica el mètode SHA-1 per SHA-256 i SHA-512. Aprecies alguna diferència? (Mostra evidències)
'''

def xifrat_sha256(text):

    hash = hashlib.sha256(text.encode())

    return hash.hexdigest()

def xifrat_sha512(text):

    hash = hashlib.sha512(text.encode())

    return hash.hexdigest()

paraula = "Hola"
frase = "ola k ase"

resultat_256 = xifrat_sha256(paraula)
resultat_512 = xifrat_sha512(frase)

# La seva diferència és la longitud del hash

print(f"La paraula '{paraula}' encriptada en SHA-256 és: \n{resultat_256} \nLongitud:",len(resultat_256))
print("\r")
print(f"la frase '{frase}' encriptada en SHA-512 és:\n{resultat_512} \nLongitud:",len(resultat_512))