import hashlib

'''
1. Implementa una funció hash del tipus SHA-1 per a encriptar la paraula hola. 
Com és la longitud de la hash de la paraula "Hola"?
'''

def xifrat_sha1(text):

    hash = hashlib.sha1(text.encode())

    return hash.hexdigest()

paraula = "Hola"

resultat = xifrat_sha1(paraula)

print(f"La paraula '{paraula}' encriptada en SHA-1 és:{resultat} \nLongitud:",len(resultat))