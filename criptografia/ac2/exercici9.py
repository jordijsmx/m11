from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP

'''
Mostra una captura de la paraula «Hola» xifrada amb clau asimètrica (RSA)
'''

def generar_claus():
    key = RSA.generate(1024)    # Generar un parell de claus RSA
    
    return key

def xifrar(clau_publica,text):

    cipher = PKCS1_OAEP.new(clau_publica)

    text_encriptat = cipher.encrypt(text.encode("utf-8"))

    return text_encriptat

def desxifrar(clau_privada,text_xifrat):

    cipher_privat = PKCS1_OAEP.new(clau_privada)

    text_desencriptat = cipher_privat.decrypt(text_xifrat)

    return text_desencriptat

if __name__ == "__main__":

    text = "Hola"
    claus = generar_claus()
    text_xifrat = xifrar(claus.publickey(),text)
    text_desxifrat = desxifrar(claus,text_xifrat)

    print("Text original:",text,"\n")
    print("Text encriptat:", text_xifrat,"\n")
    print("Longitud:",len(text_xifrat),"\n")
    print("Text desencriptat:",text_desxifrat)