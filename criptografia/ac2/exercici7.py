from Crypto.Cipher import DES
from Crypto.Random import get_random_bytes

'''
Quina és la longitud de les paraules «ola k ase» xifrada amb clau simètrica(DES)? ( Mostra
evidències)
'''

def encriptar(key,text):

    while len(text) % 8 != 0:
        text += " "
    
    cipher = DES.new(key,DES.MODE_EAX)

    text_encriptat = cipher.encrypt(text.encode())

    return text_encriptat

paraula = "ola k ase"
key = get_random_bytes(8)

text_encriptat = encriptar(key,paraula)

print("Text encriptat:", text_encriptat)
print("Longitud:",len(text_encriptat))