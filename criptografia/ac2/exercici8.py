from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
'''
Quina és la longitud de les paraules «ola k ase» xifrada amb clau simètrica (AES/ECB)? ( Mostra
evidències)
'''

def encriptar(key, text):

    while len(text) % 16 != 0:
        text += " "

    cipher = AES.new(key, AES.MODE_ECB)    

    text_encriptat = cipher.encrypt(text.encode())    

    return text_encriptat


text = "ola k ase"
key = get_random_bytes(16)
text_encriptat = encriptar(key,text)

print("Text original:",text)
print("Clau generada per a l'encriptat:",key)
print("Text encriptat:", text_encriptat)
print("Longitud:",len(text_encriptat))