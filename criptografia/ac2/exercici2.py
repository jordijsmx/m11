import hashlib

'''
Quina és la longitud de la hash de les paraules «ola k ase»? (Mostra evidències)
'''

def xifrat_sha1(text):

    hash = hashlib.sha1(text.encode())

    return hash.hexdigest()

paraula = "ola k ase"

resultat = xifrat_sha1(paraula)

print(f"La paraula '{paraula}' encriptada en SHA-1 és:{resultat} \nLongitud:",len(resultat))