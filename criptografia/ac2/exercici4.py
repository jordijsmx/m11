from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
'''
Utilitzant l’algoritme simètric AES Quina és la longitud de la clau simètrica de 
la paraula «Hola»? (Mostra evidències)
'''

def encriptar(key, text):

    cipher = AES.new(key, AES.MODE_EAX)    # Crea objecte AES amb la clau generada.

    text_encriptat = cipher.encrypt_and_digest(text)    # Encripta el missatge.
                                                            # Utilitzen un "tag" per
                                                            # garantitzar la integritat
                                                            # del missatge

    return text_encriptat # el nonce "number used once", s'utilitza per a que el xifratge
                                            # sigui vàlid per una única combinació de text original i nonce.

# Genera una clau aleatòria de 16 bytes
key = get_random_bytes(16)

# Text original
text = "Hola"
# Convertim a bytes per a que pugui ser processat per les funcions de xifratge
text_bytes = text.encode("utf-8")
#Encriptació del text
text_encriptat = encriptar(key,text_bytes)

print("Text original:",text)
print("Clau generada per a l'encriptat:",key)
print("Text encriptat:", text_encriptat)
print("Longitud:",len(text_encriptat))